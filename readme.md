# Coding Questions

## Divisible Sum Pairs (easy)

Given an array of integers and a positive integer `k`, determine the number of `(i, j)` pairs where `i < j`and `arr[i]`

+ `arr[j]` is divisible by `k`.

For example:

- `arr[] = [1, 2, 3, 4, 5, 6]`
- `k = 5`

Three pairs meet the criteria: `[1, 4], [2, 3], [4, 6]`

<details>
  <summary>Approach 1</summary>

Brute Force:

- Iterating through all list in a loop for each element
- Time complexity: `O(n^2)`:  n is the number of the element inside `arr[]`
- Space complexity: `O(1)`

</details>

<details>
  <summary>Approach 2</summary>

- Holding a bucket for the counter of the remainders the `arr[i]` when divided by `k`.
- Time complexity: `O(n)`: n is the number of the element inside `arr[]`
- Space complexity: `O(k)`

```java
class Leader {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int[] bucket = new int[k];
        int ans = 0;
        while (n-- > 0) {
            int i = sc.nextInt() % k;
            ans += bucket[(k - i) % k];
            bucket[i]++;
        }
        System.out.println(ans);
    }
}
```

</details>

## LeaderBoard

There is a leaderboard. and it is created according to Dense Ranking:

#### Dense Ranking: When two competitors have same score, they will have the same order in the leaderboard.

For example:

- The scores of 4 competitors are: `[100, 90, 90, 80]`
    - So their orders are 1st, 2nd, 2nd, 3rd respectively.

You are given scores which belong to competitors that completed the competition. and a player's scores that belongs to
that player after each game.

<table>
  <tr>
    <td>Ranks of all players</td>
    <td>1st</td>
    <td>1st</td>
    <td>2nd</td>
    <td>3rd</td>
    <td>3rd</td>
    <td>4th</td>
    <td>5th</td>
  </tr>
  <tr>
    <td>Scores of all players</td>
    <td>100</td>
    <td>100</td>
    <td>50</td>
    <td>40</td>
    <td>40</td>
    <td>20</td>
    <td>10</td>
  </tr>
</table>

The scores that taken by the player

<table>
  <tr>
    <td>5</td>
    <td>25</td>
    <td>50</td>
    <td>120</td>
  </tr>
</table>

So what would be the rank of this player after each game. According to example above, the output should be:

```
6
4
2
1
```

<details>
  <summary>Approach 1</summary>

- Create a reverse ordered set from the ranks
    - It will expose the ranks as 1st, 2nd, etc.
- Search the order of the score in that set.
- Time complexity: `~ O(n^2)`
- Space complexity: `O(n)`: n is the number of the scores in the leaderboard

</details>

<details>
  <summary>Approach 2</summary>

- Create a reverse ordered set from the ranks
    - It will expose the ranks as 1st, 2nd, etc.
- Binary search can be applied for the score that user has taken.
    - It will also provide the rank from the set if the score does not exist in the score table
- Time complexity: `O(nlogn)`
- Space complexity: `O(n)`: n is the number of the scores in the leaderboard

```java
class Approach2 {
    private static void process(Integer[] arr, Set<Integer> dd) {
        // dd is reverseOrdered set
        // arr is the score of the player on each game
        Integer[] ranks = Arrays.copyOf(dd.toArray(), dd.size(), Integer[].class);
        ArrayDeque<Integer> allRanks = new ArrayDeque<>();
        int number = 0;
        for (int i = 0; i < arr.length; i++) {
            number = Arrays.binarySearch(ranks, arr[i], Comparator.reverseOrder());
            if (number == -1) {
                allRanks.offer(1);
            } else if (number < 0) {
                allRanks.offer(-1 * number);
            } else {
                allRanks.offer(number + 1);
            }
        }

        for (Integer allRank : allRanks) {
            System.out.println(allRank);
        }
    }
}
```

</details>

## UnionFind

Write a program reads a pair p, q from the input.

- It should write the pair to the output only if the pairs it has seen to that point do not imply that p is connected to
  q.
- If the previous pairs do imply that p is connected to q, then the program should ignore the pair p q and proceed to
  read in the next pair.

<details>
<summary>General</summary>

![](./assets/connectivity.png)

```java
public abstract class Connectivity {
    private int[] id;     // access to component id (site indexed)

    public Connectivity(int N) {  // Initialize component id array.
        count = N;
        id = new int[N];
        for (int i = 0; i < N; i++) {
            id[i] = i;
        }
    }

    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    public abstract int find(int p);

    public static void main(String[] args) {
        Connectivity c = new Connectivity();
        while (!StdIn.isEmpty()) {
            int p = StdIn.readInt();
            int q = StdIn.readInt();
            if (c.connected(p, q)) {
                continue;
            }
            c.union(p, q);
        }
    }
}
```

</details>

<details>
  <summary>Approach 1 - Quick Find</summary>

```java
public class QuickFind {

    public int find(int p) {
        return id[p];
    }

    public void union(int p, int q) {  // Put p and q into the same component.
        int pID = find(p);
        int qID = find(q);
        // Nothing to do if p and q are already in the same component.
        if (pID == qID) {
            return;
        }
        // Rename p’s component to q’s name.
        for (int i = 0; i < id.length; i++) {
            if (id[i] == pID) {
                id[i] = qID;
            }
        }
    }
}
```

First approach is p and q are connected if and only if id[p] and id[q] is equal

- Find is very feasible, since always provides information about p and q in `O(1)`.
- Binding the whole components is cumbersome. Each time a new point is included, we need to iterate through all
  components to determine their root.
    - Each call to `union()` that combines two components does so by making two calls to `find()`, testing each of the N
      entries in the `id[]` array, and changing between 1 and N-1 of them.
    - It made the algorithm time complexity as `O(n^2)`
    - Space complexity: `O(n)`: n is the number of component

</details>

<details>
<summary>Approach 2 - Quick Union</summary>

```java
public class QuickUnion {
    private int find(int p) {  // Find component name.
        while (p != id[p]) {
            p = id[p];
        }
        return p;
    }

    public void union(int p, int q) {  // Give p and q the same root.
        int pRoot = find(p);
        int qRoot = find(q);
        if (pRoot == qRoot) {
            return;
        }
        id[pRoot] = qRoot;
    }
}
```

![quick-union](assets/quick-union.png)

- It is faster than the algorithm in Approach-1, because it doesn't iterate through all array when a new input comes.
- For the best case all the components bounded together in a more scattered way.
- But the worst case, again a lot of component can bind only one root, and it may cause to iterate through all array
  on `find()`.
- (1 + 2 + ... + n) ~ `O(n^2)` (worst-case) = `O(n*tree-height)`
- Space complexity: `O(n)`: n is the number of component

![](assets/worst-quick-union.png)

</details>

<details>
  <summary>Approach 3 - Weighted Quick Union </summary>

```java
public class WeightedQuickUnion {
    private int[] id;
    private int[] sz;

    public WeightedQuickUnion(int N) {
        count = N;
        id = new int[N];
        for (int i = 0; i < N; i++) {
            id[i] = i;
        }
        sz = new int[N];
        for (int i = 0; i < N; i++) {
            sz[i] = 1;
        }
    }

    private int find(int p) {  // Follow links to find a root.
        while (p != id[p]) {
            p = id[p];
        }
        return p;
    }

    public void union(int p, int q) {
        int i = find(p);
        int j = find(q);
        if (i == j) {
            return;
        }
        // Make smaller root point to larger one.
        if (sz[i] < sz[j]) {
            id[i] = j;
            sz[j] += sz[i];
        } else {
            id[j] = i;
            sz[i] += sz[j];
        }
    }
}
```

![](assets/weighted-quick-union.png)

- Quick union, there is always a possibility that the long tree can be put to the smaller tree, it increases the height
  of the tree
- If there is a height counter and if we always put smaller tree to larger one, the tree will be more balanced.
- More suitable distribution is possible.
- The graph would be more balanced
- The finding cost is `O(logn)`
- The union cost is `O(logn)`
- Total constructing the algorithm cost is `O(nlogn)`
- Space complexity: `O(2n) ~ O(n)`: n is the number of component: one is for `id[]`, the other is for `sz[]`

</details>

## Symmetric Binary Tree

Write a code that test a binary tree is symmetric or not in terms of structure and value.

<details>
  <summary>Explanation</summary>

![](assets/balanced-bt.png)
</details>

<details>
  <summary>Approach 1</summary>

- If number of the component in the Tree is even, then it should be not balanced
- Making an inorder search in the tree and hold the values inside another array.
- Iterating through that array
- Lookup

```java
class BT {
    void test() {
        int mid = arr.length / 2;
        for (i = 0; i < arr.length; i++) {
            if (i == mid) break;
            int cmp = compare(i, len - 1 - i);
            if (cmp != 0) {
                return false;
            }
        }
    }
}
```

</details>

<details>
  <summary>Approach 2</summary>

- We always know that this pattern:

```
leftSubTree.left == rightSubTree.right && leftSubTree.right == rightSubTree.left
```

- We can use this information and make recursive calls to determine whether the tree is symmetric or not.
- Time complexity: O(n)
- Space complexity: O(h): h is the height of the tree

```java
class IsSymmetric {
    public boolean isSymmetric(TreeNode root) {
        return root == null || check(root.left, root.right);
    }

    public boolean check(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        } else if (left != null && right != null) {
            return left.value == right.value &&
                    check(left.left, right, right) &&
                    check(left.right, right.left);
        }
        return false;
    }
} 
```

</details>

## Max Contiguous Subarray (dynamic programming)

Find the maximum sum of the elements inside an array, and return which subarray and the total

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
</table>

The result will be `[3, 4, 5, 6], 6`

<details>
  <summary>Approach 1</summary>

Brute Force

- Two loops:
    - Outer loop is for holding the beginning of the subarray
    - Inner loop is for iterating the array and determining the total.

- Time Complexity: `~O(n^3)`
- Space Complexity: `O(1)`

</details>

<details>
  <summary>Approach 2</summary>

- Dynamic Programming
    - Split the problem

- Time Complexity: `O(h)`: h is the height of the recursion tree
- Space Complexity: `O(n*h)`: Memoizing and stack space

![](assets/dynamic-programming.png)
</details>

<details>
  <summary>Approach 3</summary>

- Iterating through array
- Holding an additional array with the size of iterated array.
- Determine whether maximum of the `max(new value + sum before, start from the new value)`
- Time Complexity: O(n)
- Space Complexity: O(n)

</details>

<details>
  <summary>Iterations</summary>

### Iteration 1

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td><code>-2</code></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

### Iteration 2

Determine the `max(-2 + 1, 1) = 1`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td><code>1</code></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

### Iteration 3

Determine the `max(1 + -3, -3) = -2`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td><code>-2</code></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

### Iteration 4

Determine the `max(-2 + 4, 4) = 4`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td>-2</td>
    <td><code>4</code></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

### Iteration 5

Determine the `max(4 + -1, -1) = 3`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td>-2</td>
    <td>4</td>
    <td><code>3</code></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

### Iteration 6

Determine the `max(3 + 2, 2) = 5`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td>-2</td>
    <td>4</td>
    <td>3</td>
    <td><code>5</code></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

### Iteration 7

Determine the `max(5 + 1, 1) = 6`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td>-2</td>
    <td>4</td>
    <td>3</td>
    <td>5</td>
    <td><code>6</code></td>
    <td></td>
    <td></td>
  </tr>
</table>

### Iteration 8

Determine the `max(6 + -5, -5) = 1`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td>-2</td>
    <td>4</td>
    <td>3</td>
    <td>5</td>
    <td>6</td>
    <td><code>1</code></td>
    <td></td>
  </tr>
</table>

### Iteration 9

Determine the `max(1 + 4, 4) = 5`

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td>-2</td>
    <td>4</td>
    <td>3</td>
    <td>5</td>
    <td>6</td>
    <td>1</td>
    <td><code>5</code></td>
  </tr>
</table>

### Answer

<table>
  <tr>
    <td>Indexes</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td><code>3</code></td>
    <td><code>4</code></td>
    <td><code>5</code></td>
    <td><code>6</code></td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td></td>
    <td>-2</td>
    <td>1</td>
    <td>-3</td>
    <td>4</td>
    <td>-1</td>
    <td>2</td>
    <td>1</td>
    <td>-5</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Sum Array</td>
    <td>-2</td>
    <td>1</td>
    <td>-2</td>
    <td>4</td>
    <td>3</td>
    <td>5</td>
    <td><code>6</code></td>
    <td>1</td>
    <td>5</td>
  </tr>
</table>

</details>

## Next Bigger Element

Find the next element that is bigger than the original.

Example:

The array is `[6, 2, 1, 5, 4, 3, 0]`.

The next and bigger than the original array and the answer will be `[6, 2, 3, 0, 1, 4, 5]`

Other examples:

<table>
  <tr>
    <td>Original Array</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>5</td>
    <td>1</td>
    <td>3</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Next Array</td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>5</td>
    <td>2</td>
    <td>1</td>
    <td>3</td>
  </tr>
</table>

<table>
  <tr>
    <td>Original Array</td>
    <td>1</td>
    <td>1</td>
    <td>2</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Next Array</td>
    <td>1</td>
    <td>1</td>
    <td>4</td>
    <td>2</td>
  </tr>
</table>

<details>
  <summary>Approach 1</summary>

- Apply next-lexicographical-permutation-algorithm
- Iterate back to array until reach `arr[i-1] >= arr[i]`
- Take `arr[i-1]` as pivot value
- Find the value that is bigger than `arr[i-1]` beginning from the rightmost.
- Swap these
- Reverse all from `arr[i]` to `arr[arr.length - 1]`
- Time Complexity: `O(n)`
- Space Complexity: `O(1)`

![](assets/next-lexicographical.png)

```java
class Next {
    private static void process(int[] array) {
        int i = array.length - 1;
        while (i > 0 && array[i - 1] >= array[i])
            i--;

        if (i <= 0) {
            System.out.println("no answer");
            return;
        }

        int j = array.length - 1;
        while (array[j] <= array[i - 1])
            j--;
        exch(array, i - 1, j);

        j = array.length - 1;
        while (i < j) {
            exch(array, i, j);
            j--;
            i++;
        }
    }
}
```

</details>

## Clone a LinkedList

Clone a linked list with two pointers,

- One is next
- Second is random node in the list
- The last node's random pointer and the next pointer point to `null`

![](assets/clone_list.png)

<details>
  <summary>Challenge</summary>

The main challenge of this question is, we can't determine the random pointer until the node which is pointed by it
created.
Because we can't reach a specific node without traverse the linked list. No random access, all is sequential access.

</details>

<details>
  <summary>Approach 1</summary>

- 2 passes can be applied

1. All linked list is traversed and all the nodes and their clones are put in a HashMap. `{original node: clone node}`
2. All linked list is traversed and the random nodes can be taken from the HashMap, create a deep-copy of it and add its
   address to random pointer field.

- Time Complexity: `O(n)`
- Space Complexity: `O(n)`

</details>

<details>
  <summary>Approach 2</summary>

- Try to solve it with space efficient: O(1)
- Rewire them with 3 passes of the LinkedList

1. Pass
    - Next pointer of the clone node is to next node of the original node
    - Next node of the original node is the clone of it.

![](assets/first_pass.png)

2. Pass
    - Random node's next of the current original node is the node of random clone node. Bind them
        - `currentCloneNode.random = currentOriginalNode.random.next`

![](assets/second_pass.png)

3. Pass
    - `originalNode.next.next = nextOriginalNode`
    - `cloneNode.next.next = nextCloneNode`
    - Bind them.

![](assets/third_pass.png)

- Time Complexity: `O(n)`
- Space Complexity: `O(1)`

</details>